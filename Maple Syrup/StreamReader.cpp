#include "StreamReader.hpp"
#include "Utility.hpp"

char * StreamReader::AtCurrentPos( )
{
	return this->packet_buffer.data( ) + this->read_offset;
}

int StreamReader::GetCurrentOffset( )
{
	return this->read_offset;
}

void StreamReader::SetCurrentOffset( int offset )
{
	this->read_offset = offset;
}

StreamReader::StreamReader( char * buf, int len )
{
	this->packet_buffer.assign( buf, buf + len );
}

bool StreamReader::HasData( int desired_size )
{
	if ( !desired_size )
		desired_size = 1;

	return !( read_offset + desired_size > packet_buffer.size( ) );
}

int StreamReader::ReadInt( )
{
	if ( !HasData( sizeof( int ) ) )
		return -1;

	auto out = *( int * )this->AtCurrentPos( );
	read_offset += sizeof( out );
	return out;
}

char * StreamReader::GetBuffer( )
{
	return this->packet_buffer.data( );
}

std::vector<char> StreamReader::GetVector( )
{
	return this->packet_buffer;
}

int StreamReader::GetBufferLength( )
{
	return this->packet_buffer.size( );
}

int StreamReader::GetRemainingLength( )
{
	return ( this->packet_buffer.size( ) - this->read_offset );
}

std::string StreamReader::ReadString( int len )
{
	std::string out_string;
	if ( !HasData( len ) )
	{
		LogB( "Not enough data in this packet!" );
		//throw "We do not have the length for this string!";
		return out_string;
	}

	if ( len == 0 )
	{
		auto unk = this->AtCurrentPos( );
		out_string = unk;
		read_offset += out_string.length( ); // does this include the 0? kinda confused tbh. will leave us ON the zero.
	}
	else
	{
		out_string.assign( this->AtCurrentPos( ), len );
		read_offset += out_string.length( );
	}
	return out_string;
}

unsigned short StreamReader::ReadShort( )
{
	if ( !HasData( sizeof( short ) ) )
		return -1;

	auto out = *( short * )this->AtCurrentPos( );
	read_offset += sizeof( out );
	return out;
}

short StreamReader::ReadSignedShort( )
{
	if ( !HasData( sizeof( short ) ) )
		return -1;

	auto out = *( short * )this->AtCurrentPos( );
	read_offset += sizeof( out );
	return out;
}

float StreamReader::ReadFloat( )
{
	if ( !HasData( sizeof( float ) ) )
		return FLT_EPSILON; // or should i use FLT_MAX? meh.

	auto out = *( float * )this->AtCurrentPos( );
	read_offset += sizeof( out );
	return out;
}

unsigned char StreamReader::ReadByte( )
{
	if ( !HasData( ) )
		return -1;

	auto out = *( unsigned char * )this->AtCurrentPos( );
	read_offset += sizeof( out );
	return out;
}

long long StreamReader::ReadLongLong( )
{
	if ( !HasData( sizeof( long long ) ) )
		return -1;

	auto out = *( unsigned long long * )this->AtCurrentPos( );
	read_offset += sizeof( out );
	return out;
}

void StreamReader::DiscardBytes( int count )
{
	read_offset += count;
}
