#pragma once
#include "Utility.hpp"

class Gaslight
{
	uintptr_t real_base = 0, module_size = 0;
	uintptr_t fake_base = 0;
	bool initialized = false;
	HANDLE ngs_section = 0; // section handle that we use to tell our NGS bypass where the base of the game as well as our fake-game is.
public:
	Gaslight( ) {}
	~Gaslight( );
	bool Initialize( ); // Generate a full copy of maplestory.exe
	bool IsAddressInModule( uintptr_t addr );
	uintptr_t SwapAddr( uintptr_t address ); // swap out a module address ofr our fake one
};

extern Gaslight * pGaslight;