#pragma once
#include "MapleStructs.hpp"

class EncryptionManager
{
#pragma region Setup Buffers

	static constexpr unsigned char SetupBuffer1[ ] = { 0x80, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00,
														0x02, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };


	static constexpr unsigned char SetupBuffer2[ ] = { 0x39, 0x31, 0x29, 0x21, 0x19, 0x11, 0x09, 0x01, 0x3a, 0x32, 0x2a, 0x22, 0x1a, 0x12, 0x0a, 0x02, 0x3b, 0x33, 0x2b, 0x23, 0x1b, 0x13, 0x0b, 0x03,
														0x3c, 0x34, 0x2c, 0x24, 0x3f, 0x37, 0x2f, 0x27, 0x1f, 0x17, 0x0f, 0x07, 0x3e, 0x36, 0x2e, 0x26, 0x1e, 0x16, 0x0e, 0x06, 0x3d, 0x35, 0x2d, 0x25,
														0x1d, 0x15, 0x0d, 0x05, 0x1c, 0x14, 0x0c, 0x04 };

	static constexpr unsigned char SetupBuffer3[ ] = { 0x01, 0x02, 0x04, 0x06, 0x08, 0x0a, 0x0c, 0x0e, 0x0f, 0x11, 0x13, 0x15, 0x17, 0x19, 0x1b, 0x1c, 0x0e, 0x11, 0x0b, 0x18, 0x01, 0x05, 0x03, 0x1c,
														0x0f, 0x06, 0x15, 0x0a, 0x17, 0x13, 0x0c, 0x04, 0x1a, 0x08, 0x10, 0x07, 0x1b, 0x14, 0x0d, 0x02, 0x29, 0x34, 0x1f, 0x25, 0x2f, 0x37, 0x1e, 0x28,
														0x33, 0x2d, 0x21, 0x30, 0x2c, 0x31, 0x27, 0x38, 0x22, 0x35, 0x2e, 0x2a, 0x32, 0x24, 0x1d, 0x20 };
#pragma endregion

#pragma region Int array
	static constexpr unsigned char IntArray1[ ] = { 0x00, 0x04, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x04, 0x04, 0x01, 0x01, 0x04, 0x00, 0x01, 0x01, 0x04, 0x04, 0x01, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x04, 0x01, 0x01, 0x04, 0x04, 0x01, 0x01, 0x00, 0x04, 0x00, 0x00, 0x04, 0x04, 0x00, 0x01, 0x04, 0x00, 0x01, 0x01, 0x00, 0x00, 0x00, 0x01, 0x04, 0x00, 0x00, 0x00, 0x04, 0x04, 0x00, 0x00, 0x00, 0x04, 0x00, 0x01, 0x00, 0x04, 0x00, 0x01, 0x00, 0x04, 0x01, 0x00, 0x00, 0x04, 0x01, 0x00, 0x00, 0x00, 0x01, 0x01, 0x00, 0x00, 0x01, 0x01, 0x04, 0x04, 0x00, 0x01, 0x04, 0x00, 0x01, 0x00, 0x04, 0x00, 0x00, 0x01, 0x04, 0x00, 0x00, 0x01, 0x04, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x04, 0x00, 0x00, 0x04, 0x04, 0x01, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x01, 0x00, 0x04, 0x04, 0x01, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x00, 0x04, 0x01, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x04, 0x00, 0x00, 0x04, 0x00, 0x01, 0x01, 0x00, 0x00, 0x01, 0x00, 0x00, 0x04, 0x01, 0x00, 0x04, 0x00, 0x00, 0x01, 0x00, 0x04, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00, 0x04, 0x04, 0x00, 0x01, 0x04, 0x04, 0x01, 0x00, 0x04, 0x04, 0x01, 0x01, 0x04, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x01, 0x04, 0x04, 0x00, 0x01, 0x04, 0x00, 0x00, 0x01, 0x04, 0x04, 0x00, 0x00, 0x04, 0x04, 0x01, 0x00, 0x00, 0x04, 0x01, 0x01, 0x04, 0x04, 0x00, 0x00, 0x00, 0x04, 0x00, 0x01, 0x00, 0x04, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x01, 0x00, 0x00, 0x04, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x01, 0x01 };
	static constexpr unsigned char IntArray2[ ] = { 0x08, 0x02, 0x00, 0x00, 0x00, 0x02, 0x02, 0x08, 0x00, 0x00, 0x00, 0x00, 0x08, 0x00, 0x02, 0x08, 0x00, 0x02, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x08, 0x02, 0x02, 0x00, 0x00, 0x02, 0x00, 0x08, 0x08, 0x00, 0x02, 0x00, 0x08, 0x00, 0x00, 0x08, 0x08, 0x00, 0x00, 0x08, 0x00, 0x00, 0x02, 0x00, 0x08, 0x02, 0x02, 0x08, 0x08, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x08, 0x08, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x08, 0x00, 0x00, 0x00, 0x00, 0x02, 0x02, 0x08, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x02, 0x00, 0x00, 0x00, 0x02, 0x08, 0x08, 0x00, 0x02, 0x08, 0x08, 0x02, 0x02, 0x00, 0x08, 0x02, 0x00, 0x08, 0x00, 0x02, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x08, 0x02, 0x00, 0x08, 0x08, 0x00, 0x00, 0x00, 0x08, 0x02, 0x02, 0x08, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x00, 0x02, 0x02, 0x08, 0x00, 0x00, 0x00, 0x08, 0x08, 0x00, 0x02, 0x00, 0x08, 0x02, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x02, 0x02, 0x08, 0x00, 0x02, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x08, 0x00, 0x02, 0x00, 0x08, 0x02, 0x02, 0x08, 0x00, 0x02, 0x00, 0x08, 0x08, 0x00, 0x00, 0x08, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x00, 0x02, 0x08, 0x08, 0x02, 0x00, 0x08, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x08, 0x08, 0x02, 0x02, 0x08, 0x08, 0x00, 0x00, 0x00, 0x08, 0x02, 0x02, 0x00, 0x00, 0x02, 0x02, 0x00, 0x08, 0x00, 0x00, 0x08, 0x00, 0x00, 0x02, 0x08, 0x08, 0x02, 0x00, 0x08, 0x08, 0x02, 0x00, 0x00, 0x00, 0x00, 0x02, 0x08, 0x08, 0x02, 0x02, 0x00, 0x08, 0x00, 0x00, 0x00, 0x08, 0x00, 0x02, 0x08, 0x00, 0x02, 0x02, 0x00 };
	static constexpr unsigned char IntArray3[ ] = { 0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x08, 0x02, 0x00, 0x00, 0x08, 0x02, 0x00, 0x01, 0x00, 0x42, 0x00, 0x00, 0x08, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x08, 0x02, 0x00, 0x01, 0x08, 0x40, 0x00, 0x00, 0x08, 0x00, 0x00, 0x01, 0x00, 0x02, 0x00, 0x01, 0x08, 0x40, 0x00, 0x01, 0x00, 0x42, 0x00, 0x00, 0x08, 0x42, 0x00, 0x01, 0x08, 0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x08, 0x40, 0x00, 0x00, 0x08, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x40, 0x00, 0x01, 0x08, 0x42, 0x00, 0x01, 0x08, 0x42, 0x00, 0x01, 0x00, 0x02, 0x00, 0x00, 0x08, 0x42, 0x00, 0x01, 0x00, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x42, 0x00, 0x01, 0x08, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x42, 0x00, 0x01, 0x08, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x01, 0x00, 0x42, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x08, 0x02, 0x00, 0x01, 0x00, 0x42, 0x00, 0x01, 0x08, 0x40, 0x00, 0x01, 0x00, 0x02, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x08, 0x42, 0x00, 0x01, 0x08, 0x02, 0x00, 0x01, 0x08, 0x40, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x08, 0x42, 0x00, 0x01, 0x08, 0x42, 0x00, 0x01, 0x08, 0x00, 0x00, 0x00, 0x00, 0x42, 0x00, 0x01, 0x08, 0x42, 0x00, 0x00, 0x08, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x40, 0x00, 0x00, 0x00, 0x42, 0x00, 0x01, 0x08, 0x00, 0x00, 0x01, 0x00, 0x02, 0x00, 0x01, 0x00, 0x40, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x40, 0x00, 0x01, 0x08, 0x02, 0x00, 0x01, 0x00, 0x40 };
	static constexpr unsigned char IntArray4[ ] = { 0x00, 0x00, 0x20, 0x00, 0x02, 0x00, 0x20, 0x04, 0x02, 0x08, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x02, 0x08, 0x00, 0x04, 0x02, 0x08, 0x20, 0x00, 0x00, 0x08, 0x20, 0x04, 0x02, 0x08, 0x20, 0x04, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x04, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x02, 0x00, 0x20, 0x04, 0x02, 0x08, 0x00, 0x00, 0x00, 0x08, 0x00, 0x04, 0x02, 0x08, 0x20, 0x00, 0x02, 0x00, 0x20, 0x00, 0x00, 0x08, 0x00, 0x04, 0x02, 0x00, 0x00, 0x04, 0x00, 0x00, 0x20, 0x04, 0x00, 0x08, 0x20, 0x04, 0x02, 0x00, 0x20, 0x00, 0x00, 0x00, 0x20, 0x04, 0x00, 0x08, 0x00, 0x00, 0x02, 0x08, 0x00, 0x00, 0x02, 0x08, 0x20, 0x04, 0x00, 0x08, 0x20, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x08, 0x20, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x08, 0x20, 0x00, 0x00, 0x00, 0x20, 0x00, 0x02, 0x08, 0x00, 0x04, 0x02, 0x08, 0x00, 0x04, 0x02, 0x00, 0x20, 0x04, 0x02, 0x00, 0x20, 0x04, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x08, 0x00, 0x04, 0x00, 0x00, 0x20, 0x00, 0x00, 0x08, 0x20, 0x04, 0x02, 0x08, 0x00, 0x00, 0x02, 0x08, 0x20, 0x00, 0x00, 0x08, 0x20, 0x04, 0x02, 0x08, 0x00, 0x00, 0x02, 0x00, 0x00, 0x04, 0x02, 0x08, 0x20, 0x04, 0x00, 0x00, 0x20, 0x04, 0x00, 0x08, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x08, 0x20, 0x04, 0x00, 0x00, 0x00, 0x00, 0x02, 0x08, 0x20, 0x00, 0x00, 0x00, 0x20, 0x04, 0x00, 0x08, 0x00, 0x00, 0x02, 0x00, 0x00, 0x04, 0x00, 0x08, 0x00, 0x04, 0x00, 0x08, 0x00, 0x00, 0x02, 0x00, 0x20, 0x00 };
	static constexpr unsigned char IntArray5[ ] = { 0x20, 0x80, 0x10, 0x80, 0x00, 0x80, 0x00, 0x80, 0x00, 0x80, 0x00, 0x00, 0x20, 0x80, 0x10, 0x00, 0x00, 0x00, 0x10, 0x00, 0x20, 0x00, 0x00, 0x00, 0x20, 0x00, 0x10, 0x80, 0x20, 0x80, 0x00, 0x80, 0x20, 0x00, 0x00, 0x80, 0x20, 0x80, 0x10, 0x80, 0x00, 0x80, 0x10, 0x80, 0x00, 0x00, 0x00, 0x80, 0x00, 0x80, 0x00, 0x80, 0x00, 0x00, 0x10, 0x00, 0x20, 0x00, 0x00, 0x00, 0x20, 0x00, 0x10, 0x80, 0x00, 0x80, 0x10, 0x00, 0x20, 0x00, 0x10, 0x00, 0x20, 0x80, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x80, 0x00, 0x00, 0x20, 0x80, 0x10, 0x00, 0x00, 0x00, 0x10, 0x80, 0x20, 0x00, 0x10, 0x00, 0x20, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x10, 0x00, 0x20, 0x80, 0x00, 0x00, 0x00, 0x80, 0x10, 0x80, 0x00, 0x00, 0x10, 0x80, 0x20, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x80, 0x10, 0x00, 0x20, 0x00, 0x10, 0x80, 0x00, 0x00, 0x10, 0x00, 0x20, 0x80, 0x00, 0x80, 0x00, 0x00, 0x10, 0x80, 0x00, 0x80, 0x10, 0x80, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x10, 0x80, 0x00, 0x80, 0x00, 0x80, 0x20, 0x00, 0x00, 0x00, 0x20, 0x80, 0x10, 0x80, 0x20, 0x80, 0x10, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x20, 0x80, 0x00, 0x00, 0x00, 0x80, 0x10, 0x80, 0x00, 0x00, 0x10, 0x00, 0x20, 0x00, 0x00, 0x80, 0x20, 0x00, 0x10, 0x00, 0x20, 0x80, 0x00, 0x80, 0x20, 0x00, 0x00, 0x80, 0x20, 0x00, 0x10, 0x00, 0x00, 0x80, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x80, 0x20, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x20, 0x00, 0x10, 0x80, 0x20, 0x80, 0x10, 0x80, 0x00, 0x80, 0x10, 0x00 };
	static constexpr unsigned char IntArray6[ ] = { 0x01, 0x20, 0x80, 0x00, 0x81, 0x20, 0x00, 0x00, 0x81, 0x20, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x80, 0x20, 0x80, 0x00, 0x81, 0x00, 0x80, 0x00, 0x01, 0x00, 0x80, 0x00, 0x01, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x80, 0x00, 0x00, 0x20, 0x80, 0x00, 0x81, 0x20, 0x80, 0x00, 0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x80, 0x00, 0x01, 0x00, 0x80, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x01, 0x20, 0x80, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x01, 0x20, 0x00, 0x00, 0x80, 0x20, 0x00, 0x00, 0x81, 0x00, 0x80, 0x00, 0x01, 0x00, 0x00, 0x00, 0x80, 0x20, 0x00, 0x00, 0x80, 0x00, 0x80, 0x00, 0x00, 0x20, 0x00, 0x00, 0x80, 0x20, 0x80, 0x00, 0x81, 0x20, 0x80, 0x00, 0x81, 0x00, 0x00, 0x00, 0x80, 0x00, 0x80, 0x00, 0x01, 0x00, 0x80, 0x00, 0x00, 0x20, 0x80, 0x00, 0x81, 0x20, 0x80, 0x00, 0x81, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x80, 0x00, 0x80, 0x20, 0x00, 0x00, 0x80, 0x00, 0x80, 0x00, 0x81, 0x00, 0x80, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x20, 0x80, 0x00, 0x81, 0x20, 0x00, 0x00, 0x81, 0x20, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00, 0x81, 0x20, 0x80, 0x00, 0x81, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x01, 0x00, 0x80, 0x00, 0x01, 0x20, 0x00, 0x00, 0x80, 0x20, 0x80, 0x00, 0x81, 0x00, 0x80, 0x00, 0x01, 0x20, 0x00, 0x00, 0x80, 0x20, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x01, 0x20, 0x80, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x20, 0x00, 0x00, 0x80, 0x20, 0x80, 0x00 };
	static constexpr unsigned char IntArray7[ ] = { 0x10, 0x00, 0x00, 0x20, 0x00, 0x00, 0x40, 0x20, 0x00, 0x40, 0x00, 0x00, 0x10, 0x40, 0x40, 0x20, 0x00, 0x00, 0x40, 0x20, 0x10, 0x00, 0x00, 0x00, 0x10, 0x40, 0x40, 0x20, 0x00, 0x00, 0x40, 0x00, 0x00, 0x40, 0x00, 0x20, 0x10, 0x40, 0x40, 0x00, 0x00, 0x00, 0x40, 0x00, 0x10, 0x00, 0x00, 0x20, 0x10, 0x00, 0x40, 0x00, 0x00, 0x40, 0x00, 0x20, 0x00, 0x00, 0x00, 0x20, 0x10, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x40, 0x00, 0x10, 0x40, 0x00, 0x20, 0x00, 0x40, 0x00, 0x00, 0x00, 0x40, 0x40, 0x00, 0x10, 0x40, 0x00, 0x20, 0x10, 0x00, 0x00, 0x00, 0x10, 0x00, 0x40, 0x20, 0x10, 0x00, 0x40, 0x20, 0x00, 0x00, 0x00, 0x00, 0x10, 0x40, 0x40, 0x00, 0x00, 0x40, 0x40, 0x20, 0x10, 0x40, 0x00, 0x00, 0x00, 0x40, 0x40, 0x00, 0x00, 0x40, 0x40, 0x20, 0x00, 0x00, 0x00, 0x20, 0x00, 0x40, 0x00, 0x20, 0x10, 0x00, 0x00, 0x00, 0x10, 0x00, 0x40, 0x20, 0x00, 0x40, 0x40, 0x00, 0x10, 0x40, 0x40, 0x20, 0x00, 0x00, 0x40, 0x00, 0x10, 0x40, 0x00, 0x00, 0x10, 0x00, 0x00, 0x20, 0x00, 0x00, 0x40, 0x00, 0x00, 0x40, 0x00, 0x20, 0x00, 0x00, 0x00, 0x20, 0x10, 0x40, 0x00, 0x00, 0x10, 0x00, 0x00, 0x20, 0x10, 0x40, 0x40, 0x20, 0x00, 0x40, 0x40, 0x00, 0x00, 0x00, 0x40, 0x20, 0x10, 0x40, 0x40, 0x00, 0x00, 0x40, 0x40, 0x20, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x40, 0x20, 0x10, 0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x00, 0x40, 0x20, 0x10, 0x40, 0x40, 0x00, 0x00, 0x40, 0x00, 0x00, 0x10, 0x00, 0x40, 0x00, 0x10, 0x40, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x40, 0x20, 0x00, 0x00, 0x00, 0x20, 0x10, 0x00, 0x40, 0x00, 0x10, 0x40, 0x00, 0x20 };
	static constexpr unsigned char IntArray8[ ] = { 0x40, 0x10, 0x00, 0x10, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x40, 0x10, 0x04, 0x10, 0x00, 0x00, 0x00, 0x10, 0x40, 0x10, 0x00, 0x10, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x40, 0x00, 0x04, 0x00, 0x00, 0x00, 0x04, 0x10, 0x40, 0x10, 0x04, 0x10, 0x00, 0x10, 0x04, 0x00, 0x00, 0x10, 0x04, 0x10, 0x40, 0x10, 0x04, 0x00, 0x00, 0x10, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x10, 0x40, 0x00, 0x00, 0x10, 0x00, 0x10, 0x00, 0x10, 0x40, 0x10, 0x00, 0x00, 0x00, 0x10, 0x04, 0x00, 0x40, 0x00, 0x04, 0x00, 0x40, 0x00, 0x04, 0x10, 0x00, 0x10, 0x04, 0x10, 0x40, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x04, 0x10, 0x40, 0x00, 0x00, 0x10, 0x00, 0x10, 0x00, 0x10, 0x40, 0x10, 0x04, 0x00, 0x00, 0x00, 0x04, 0x00, 0x40, 0x10, 0x04, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x10, 0x04, 0x10, 0x00, 0x10, 0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x40, 0x00, 0x04, 0x10, 0x00, 0x10, 0x00, 0x00, 0x40, 0x10, 0x04, 0x00, 0x00, 0x10, 0x00, 0x10, 0x40, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x10, 0x00, 0x00, 0x04, 0x10, 0x40, 0x00, 0x04, 0x10, 0x00, 0x00, 0x00, 0x10, 0x00, 0x00, 0x04, 0x00, 0x40, 0x10, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x40, 0x10, 0x04, 0x10, 0x40, 0x00, 0x04, 0x00, 0x40, 0x00, 0x00, 0x10, 0x00, 0x00, 0x04, 0x10, 0x00, 0x10, 0x00, 0x10, 0x40, 0x10, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x40, 0x10, 0x04, 0x10, 0x00, 0x10, 0x04, 0x00, 0x00, 0x10, 0x04, 0x00, 0x40, 0x10, 0x00, 0x00, 0x40, 0x10, 0x00, 0x00, 0x40, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00, 0x10, 0x04, 0x10 };
#pragma endregion

	const char * maple_key = "M@PleStoryMaPLe!";
	const int * encrypted_pids = nullptr;

	int * decoding_buffer1 = nullptr, * decoding_buffer2 = nullptr;

	char * SetupZeroOneBuffer( int maple_key_offset = 0 );

	void StepTwoA( char * buff, char value );
	void StepTwoB( char * buff );
	int * StepTwo( char * buff );
	int GetXorValueFromArrays( int index1, int index2 );
	void DecryptionRelatedStuff2( int & val_one, int & val_two );
	void DecryptionRelatedStuff3( int & val_one, int & val_two );
	void DecryptWithIntArray( int * buffer, int & val_one, int & val_two );

	void StepThree( char * buff );

public:
	EncryptionManager( );
	int* DecodeBuffer( int data_size );
	void SetEncryptedPidsBuffer( const char * buff );

};


extern EncryptionManager * pEncryptionManager;