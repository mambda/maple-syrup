#include "Utility.hpp"
#include "global_defines.hpp"
#include <fstream>
#include <sstream>

namespace Utility
{
	void SetByte( uintptr_t loc, unsigned char byte )
	{
		DWORD old = 0;
		VirtualProtect( ( void * )loc, 1, PAGE_EXECUTE_READWRITE, &old );
		*( unsigned char * )loc = byte;
		VirtualProtect( ( void * )loc, 1, old, &old );

		//auto success = FlushInstructionCache( ( HANDLE )-1, ( void * )loc, 0 );
	}

	uintptr_t GetModuleSize( uintptr_t base )
	{
		MEMORY_BASIC_INFORMATION region = { 0 };
		ULONG out_size = 0;
		auto stat = NtQueryVirtualMemory( ( HANDLE )-1, ( void * )base, MemoryRegionInformation, &region, sizeof( region ), &out_size );
		if ( stat != 0 )
		{
			if ( stat == 0xC0000004 )
			{
				Log( "Actual size is %d not %d", out_size, sizeof( region ) );
			}

			Log( "NTQVM ERROR: %X", stat );
			return 0;
		}

		return region.RegionSize;
	}

	static int count = 0;

	std::string GenerateDumpName( int size, int id )
	{
		std::stringstream ss;
		ss << ms_defines::dump_loc << count++ << "_ID_" << std::hex << id << "_SIZE_" << std::hex << size << ".bin";
		return ss.str( );
	}

	std::string GenerateDumpName( int size, const char * name )
	{
		std::stringstream ss;
		ss << ms_defines::dump_loc << count++ << "_" << name << "_SIZE_" << std::hex << size << ".bin";
		return ss.str( );
	}

	std::wstring GetModuleForAddress( uintptr_t addr, int pid )
	{
		char buffer[ MAX_PATH * 2 ] = { 0 };
		PUNICODE_STRING us = 0;
		std::wstring big_name;
		SIZE_T ret_len = 0;
		HANDLE hProc = OpenProcess( PROCESS_QUERY_INFORMATION, 0, pid );
		auto stat = NtQueryVirtualMemory( hProc, ( void * )addr, MemoryMappedFilenameInformation, &buffer, sizeof( buffer ), &ret_len );
		CloseHandle( hProc );
		if ( stat != STATUS_SUCCESS )
		{
			//LogB( "Failed to query module name for addr %p with fail code %X", addr, stat );
		}
		else
		{
			us = ( PUNICODE_STRING )( buffer );
			big_name = us->Buffer;
			auto fname_pos = big_name.find_last_of( L"\\" );
			big_name = big_name.substr( fname_pos + 1 );
		}

		return us ? big_name : std::wstring( L"PRIVATE" );
	}

	uintptr_t GetModuleBase( uintptr_t addr )
	{
		MEMORY_BASIC_INFORMATION buffer = { 0 };
		SIZE_T ret_len = 0;
		auto stat = NtQueryVirtualMemory( ( HANDLE )-1, ( void * )addr, MemoryBasicInformation, &buffer, sizeof( buffer ), &ret_len );
		if ( stat != STATUS_SUCCESS )
		{
			//LogB( "Failed to query module name for addr %p with fail code %X", addr, stat );
		}

		return ( uintptr_t )buffer.AllocationBase;
	}

	void DumpContext( CONTEXT ctx )
	{
		LogB( "\nEIP: %p", ctx.Eip );
		LogB( "ESP: %p", ctx.Esp );
		LogB( "EBP: %p\n", ctx.Ebp );
	}

	DWORD __stdcall GenerateThreadTrace( HANDLE hThread )
	{
		SuspendThread( hThread );

		CONTEXT ctx = { 0 };
		ctx.ContextFlags = CONTEXT_ALL;
		auto stat = NtGetContextThread( hThread, &ctx );
		ResumeThread( hThread );

		DumpContext( ctx );
		return 0;
	}
}